package addressBookApp.model;
import org.apache.log4j.Logger;
import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


@XmlRootElement(name = "addressBook")
public class AddressBook {

    @XmlTransient
    final static Logger logger = Logger.getLogger(AddressBook.class);

    private String name;

    @XmlElement(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private List<Person> peopleList;

    public AddressBook() {
        this.peopleList = new ArrayList<Person>();
    }

    public AddressBook(String name) {
        this.name = name;

        this.peopleList = new ArrayList<Person>();
    }

    @XmlElements(@XmlElement)
    public List<Person> getPeopleList() {
        return peopleList;
    }

    public void setPeopleList(List<Person> peopleList) {
        this.peopleList = peopleList;
    }

    public AddressBook findPeopleByCity(String city) {
        AddressBook addressBook = new AddressBook(this.name + ": people who live in " + city);
        if (logger.isInfoEnabled()) {
            logger.info("There was returned people list who live in " + city);
        }
        peopleList.stream()
                .filter(p -> p.getAddress().getCity().equals(city))
                .forEach(p -> addressBook.getPeopleList().add(p));

        return addressBook;
    }
}
