package addressBookApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
@ComponentScan(basePackages = "addressBookApp")

    public class AppMain {

        public static void main(String[] args) {
            SpringApplication.run(AppMain.class, args);
        }

    }

