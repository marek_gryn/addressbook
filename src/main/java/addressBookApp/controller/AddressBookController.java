package addressBookApp.controller;

import addressBookApp.common.MyXMLHandler;

import addressBookApp.model.AddressBook;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.Optional;

@Controller
@RequestMapping("/addressBook")
public class AddressBookController {

    private final MyXMLHandler adressBookXMLHandler = new MyXMLHandler(AddressBook.class);
    private final String folder = "XML_FILES\\";


    public AddressBookController() throws JAXBException {
    }

    @GetMapping(value = "/get")
    public ResponseEntity<AddressBook> get(@RequestParam("path") String fileName) {

        Optional optionalAddressBook = adressBookXMLHandler.loadFromFile(new File(folder + fileName + ".xml"));

        AddressBook addressBook = null;
        if (optionalAddressBook.isPresent()) {
            addressBook = (AddressBook) optionalAddressBook.get();
        }

        return ResponseEntity
                .ok()
                .body(addressBook);
    }

    @GetMapping(value = "/getByCity", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AddressBook> getByCity(@RequestParam("path") String fileName, @RequestParam("city") String city) {

        Optional optionalAddressBook = adressBookXMLHandler.loadFromFile(new File(folder + fileName + ".xml"));

        AddressBook addressBook = null;
        if (optionalAddressBook.isPresent()) {
            addressBook = (AddressBook) optionalAddressBook.get();
            addressBook = addressBook.findPeopleByCity(city);
        }

        return ResponseEntity
                .ok()
                .body(addressBook);
    }

    @PostMapping(value = "/save")
    public ResponseEntity<Void> addAddressBook(@RequestBody AddressBook addressBook, @RequestParam("path") String fileName) {

        adressBookXMLHandler.saveToFile(addressBook, new File(folder + fileName + ".xml"));

        return ResponseEntity
                .ok()
                .build();
    }

    @DeleteMapping(value = "/delete")
    public ResponseEntity<Void> deleteAddressBook(@RequestParam("path") String fileName) {
        Path filePath = FileSystems.getDefault().getPath(folder, fileName + ".xml");
        try {
            Files.delete(filePath);
        } catch (NoSuchFileException | DirectoryNotEmptyException x) {
            return ResponseEntity
                    .notFound()
                    .build();
        } catch (IOException e) {
            System.err.println(e);
        }
        return ResponseEntity
                .ok()
                .build();
    }
}

