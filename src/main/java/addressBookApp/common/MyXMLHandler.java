package addressBookApp.common;

import org.apache.log4j.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.Optional;

public class MyXMLHandler {
    final static Logger logger = Logger.getLogger(MyXMLHandler.class);

    private Class clazz;
    private Marshaller jaxbMarshaller;
    private Unmarshaller jaxbUnmarshaller;

    public MyXMLHandler(Class clazz) throws JAXBException {
        this.clazz = clazz;
        JAXBContext jaxbContext = JAXBContext.newInstance(this.clazz);
        this.jaxbMarshaller = jaxbContext.createMarshaller();
        this.jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        this.jaxbUnmarshaller = jaxbContext.createUnmarshaller();
    }

    public void printToConsole(Object o) {
        if (o.getClass() == clazz) {
            try {
                jaxbMarshaller.marshal(o, System.out);
            } catch (JAXBException e) {
                System.out.println("Not possible to print this object");
            }
        } else {
            System.out.println("Class of given object o is not applied for this instance of addressBookApp.common.MyXMLHandler");
        }

    }

    public void saveToFile(Object o, File file) {
        if (o.getClass() == clazz) {
            try {
                jaxbMarshaller.marshal(o, file);
                if (logger.isInfoEnabled()) {
                    logger.info("Object has been saved to file: " + file.getAbsolutePath());
                }
            } catch (Exception e) {
                logger.error("Not possible to save this object", e);
            }
        } else {
            logger.error("Class of given object o is not applied for this instance of addressBookApp.common.MyXMLHandler");

        }
    }

    public Optional<Object> loadFromFile(File file) {
        try {
            Object o = jaxbUnmarshaller.unmarshal(file);
            if (o != null && o.getClass() == clazz) {
                if (logger.isInfoEnabled()) {
                    logger.info("There was loaded file from path: " + file.getAbsolutePath());
                }
                return Optional.of(o);
            }
        } catch (Exception e) {
            logger.error("Not possible to load object from file", e);
        }
        logger.error("Class of given object o is not applied for this instance of addressBookApp.common.MyXMLHandler or object is null ");
        return Optional.empty();
    }
}
